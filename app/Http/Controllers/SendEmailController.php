<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
class SendEmailController extends Controller
{
     function index(){
        return view('send_email');
    }
    function send(Request $request){
$data=array(
    'name'=>$request->name,
    'message'=>$request->message
);
Mail::to('web-tutorial@programmer.mailtrap.io')->send(new SendMail ($data));
return redirect('send_email')->with('success','the email send');
   }
}
